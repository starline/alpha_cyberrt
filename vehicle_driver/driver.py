#!/usr/bin/env python3

#############################################################################
# Copyright 2021 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import os
import sys
import time
import json
from math import pi, sqrt, copysign, isnan

from cyber.python.cyber_py3 import cyber, cyber_time
from modules.common_msgs.chassis_msgs import chassis_pb2, chassis_detail_pb2
from modules.common_msgs.config_msgs import vehicle_config_pb2
from modules.common_msgs.control_msgs import control_cmd_pb2
import modules.tools.common.proto_utils as proto_utils

from alpha import Vehicle, VehicleParams, Spinner
from alpha import available_controllers
from alpha import log

from alpha.protocol import VEHICLE_MODE


CHASSIS_PUB_RATE        = 100
CHASSIS_DETAIL_PUB_RATE = 100
VEHICLE_ODOM_PUB_RATE   = 25

VEHICLE_CONTROLLER_RATE = 70

APOLLO_VEHICLE_PARAM_PATH = "/apollo/modules/common/data/vehicle_param.pb.txt"
ALPHA_PARAM_PATH          = "/apollo/modules/common/data/alpha_param.json"
SIGMA_PORT                = '/dev/alpha'


class VehicleCyberRTWrapper:

    def __init__(self):

        cyber.init()

        # Apollo params loading -----------------------------------------------

        apollo_vehicle_config = vehicle_config_pb2.VehicleConfig()
        proto_utils.get_pb_from_text_file(APOLLO_VEHICLE_PARAM_PATH, apollo_vehicle_config)
        apollo_vehicle_params = apollo_vehicle_config.vehicle_param

        self.vehicle_params = VehicleParams()
        self.vehicle_params["wheel_radius"]     = apollo_vehicle_params.wheel_rolling_radius
        self.vehicle_params["wheelbase"]        = apollo_vehicle_params.wheel_base
        self.vehicle_params["steering_ratio"]   = apollo_vehicle_params.steer_ratio
        self.vehicle_params["max_sw_angle"]     = apollo_vehicle_params.max_steer_angle / pi * 180
        self.vehicle_params["max_sw_rate"]      = apollo_vehicle_params.max_steer_angle_rate / pi * 180
        self.vehicle_params["max_acceleration"] = apollo_vehicle_params.max_acceleration
        self.vehicle_params["max_deceleration"] = abs(apollo_vehicle_params.max_deceleration)

        # Alpha params loading ------------------------------------------------

        alpha_params = None

        global SIGMA_PORT

        if os.path.exists(ALPHA_PARAM_PATH):
            with open(ALPHA_PARAM_PATH) as alpha_params_file:
                alpha_params = json.load(alpha_params_file)

        if alpha_params:

            if "sigma_port" in alpha_params:
                SIGMA_PORT = alpha_params["sigma_port"]

            if "control_type" in alpha_params:
                self.vehicle_params["control_type"] = alpha_params["control_type"]

            if ("controller_name" in alpha_params and
                alpha_params["controller_name"] in available_controllers()):

                self.vehicle_params["controller_name"] = alpha_params["controller_name"]

                self.vehicle_params["controller_params"] = available_controllers()[alpha_params["controller_name"]].Params()

            if "controller_params" in alpha_params:
                self.vehicle_params["controller_params"].update(alpha_params["controller_params"])

            if "controller_rate" in alpha_params:
                self.vehicle_params["controller_rate"] = alpha_params["controller_rate"]

            if "sigma_port" in alpha_params:
                SIGMA_PORT = alpha_params["sigma_port"]

        # Start to communicate with alpha on vehicle --------------------------

        self.vehicle = Vehicle(SIGMA_PORT, self.vehicle_params)

        self._prev_control_driving_mode = chassis_pb2.Chassis.COMPLETE_MANUAL

        self._prev_driver_mode = VEHICLE_MODE.UNKNOWN
        self._driver_mode      = VEHICLE_MODE.UNKNOWN

        self._prev_control_estop = False
        self._prev_control_led   = False

        self.last_throttle_cmd = 0.0
        self.last_sw_angle_cmd = 0.0

        node = cyber.Node("alpha")

        self._control_sub = node.create_reader('/apollo/control',
                                               control_cmd_pb2.ControlCommand,
                                               self.control_cb)

        self._chassis_pub = node.create_writer('/apollo/canbus/chassis',
                                               chassis_pb2.Chassis)

        self._chassis_spn = Spinner(self._chassis_loop, CHASSIS_PUB_RATE)
        self._chassis_spn.start()

        # self._chassis_detail_pub = node.create_writer('/apollo/canbus/chassis_detail',
        #                                               chassis_detail_pb2.ChassisDetail)


    def control_cb(self, msg):

        driver_mode = self._driver_mode

        has_mode  = msg.HasField("driving_mode")
        has_gear  = msg.HasField("gear_location")
        has_estop = msg.HasField("parking_brake")

        has_signal = msg.HasField("signal")

        has_tsignal = False
        has_elight  = False
        has_led     = False

        if has_signal:
            has_tsignal = msg.signal.HasField("turn_signal")
            has_elight  = msg.signal.HasField("emergency_light")
            has_led     = msg.signal.HasField("low_beam")

        # Activate TAKEOVER from Control channel ------------------------------

        if has_mode:

            if self._prev_control_driving_mode != msg.driving_mode:

                if msg.driving_mode == chassis_pb2.Chassis.COMPLETE_AUTO_DRIVE:

                    if has_gear and msg.gear_location == chassis_pb2.Chassis.GEAR_REVERSE:
                        self.vehicle.reverse(False)
                    else:
                        self.vehicle.drive(False)

                else:
                    self.vehicle.manual()

            self._prev_control_driving_mode = msg.driving_mode

        # Check if TAKEOVER is Active -----------------------------------------

        on_takeover = (driver_mode == VEHICLE_MODE.DRIVE or
                       driver_mode == VEHICLE_MODE.REVERSE)

        # Check if TAKEOVER Mode was changed ----------------------------------

        if driver_mode != self._prev_driver_mode:

            if (on_takeover):
                self.vehicle.start_controller()
                self.vehicle.start_send_vehicle_cmd()
            else:
                self.vehicle.stop_send_vehicle_cmd()
                self.vehicle.stop_controller()

            self._prev_driver_mode = driver_mode

        if on_takeover:

            # THROTTLE AND STEERING WHEEL
            self.last_throttle_cmd = (-1 * msg.brake) if msg.brake > 0 else msg.throttle

            if isnan(self.last_throttle_cmd):
                self.last_throttle_cmd = 0

            sw_angle = msg.steering_target * 0.01 * self.vehicle_params["max_sw_angle"]

            if isnan(sw_angle):
                sw_angle = 0

            if msg.HasField("steering_rate"):
                sw_rate = msg.steering_rate * 0.01 * self.vehicle_params["max_sw_rate"]
            else:
                sw_rate = None

            self.vehicle.move(self.last_throttle_cmd)
            self.vehicle.steer(sw_angle, sw_rate)

            if has_gear:

                mode_to_be_set = self.apollo_gear_to_alpha_mode(msg.gear_location)

                if mode_to_be_set != driver_mode:

                    if mode_to_be_set == VEHICLE_MODE.DRIVE:
                        self.vehicle.drive(False)

                    elif mode_to_be_set == VEHICLE_MODE.REVERSE:
                        self.vehicle.reverse(False)

        # EMERGENCY STOP ------------------------------------------------------

        if has_estop:
            if msg.parking_brake != self._prev_control_estop:

                if msg.parking_brake:
                    self.vehicle.emergency_stop()

                else:
                    self.vehicle.recover()

                self._prev_control_estop = msg.parking_brake

        # LED -----------------------------------------------------------------

        if has_led:
            if self._prev_control_led != msg.signal.low_beam:

                if msg.signal.low_beam:
                    self.vehicle.led_on()

                else:
                    self.vehicle.led_off()

            self._prev_control_led = msg.signal.low_beam


    def apollo_gear_to_alpha_mode(self, gear):

        if gear == chassis_pb2.Chassis.GEAR_REVERSE:
            return VEHICLE_MODE.REVERSE

        elif gear == chassis_pb2.Chassis.GEAR_DRIVE:
            return VEHICLE_MODE.DRIVE

        else:
            return VEHICLE_MODE.UNKNOWN


    def alpha_mode_to_apollo(self, mode):

        if mode == VEHICLE_MODE.DRIVE:
            return (chassis_pb2.Chassis.COMPLETE_AUTO_DRIVE,
                    chassis_pb2.Chassis.GEAR_DRIVE)

        elif mode == VEHICLE_MODE.REVERSE:
            return (chassis_pb2.Chassis.COMPLETE_AUTO_DRIVE,
                    chassis_pb2.Chassis.GEAR_REVERSE)

        else:
            return (chassis_pb2.Chassis.COMPLETE_MANUAL,
                    chassis_pb2.Chassis.GEAR_NONE)


    # def update_vehicle_driver_state(self):

        # self._driver_mode = self.vehicle.get_mode()

        # vehicle_speed = self.vehicle.get_vehicle_speed()
        #
        # sw_angle, sw_rate = self.vehicle.get_steering_wheel_angle_and_velocity()


    def _chassis_loop(self):

        # self.update_vehicle_driver_state()

        self._driver_mode = self.vehicle.get_mode()

        vehicle_speed = self.vehicle.get_vehicle_speed()
        sw_angle, sw_rate = self.vehicle.get_steering_wheel_angle_and_velocity()
        gas_pedal = self.vehicle.get_gas_pedal()
        brake_pedal = self.vehicle.get_brake_pedal()

        sw_angle = sw_angle * 100.0 / self.vehicle_params["max_sw_angle"]

        chassis_msg = chassis_pb2.Chassis()

        chassis_msg.speed_mps           = vehicle_speed
        chassis_msg.steering_percentage = sw_angle

        if self.last_throttle_cmd > 0:
            chassis_msg.throttle_percentage = self.last_throttle_cmd
            chassis_msg.brake_percentage    = 0.0
        else:
            chassis_msg.throttle_percentage = 0.0
            chassis_msg.brake_percentage    = abs(self.last_throttle_cmd)

        amode, agear = self.alpha_mode_to_apollo(self._driver_mode)

        chassis_msg.driving_mode  = amode
        chassis_msg.gear_location = agear

        chassis_msg.header.timestamp_sec = cyber_time.Time.now().to_sec()

        self._chassis_pub.write(chassis_msg)


    # def publish_chassis_detail(self):
    #     pass


    def spin(self):
        while cyber.ok():
            time.sleep(1)


if __name__ == "__main__":
    vehicle_driver = VehicleCyberRTWrapper()
    vehicle_driver.spin()
